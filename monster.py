import os
import sys
import psutil
import signal 
import pandas as pd
from firebase import firebase
from bs4 import BeautifulSoup
from selenium import webdriver
from requests.compat import quote_plus
from selenium.webdriver.chrome.options import Options

tags = [
        '.net', 'agile', 'ai', 'ajax', 'alexa', 'algorithms', 'android', 'angular', 'apache', 
        'api', 'app', 'arduino', 'asp', 'asp.net', 'aws', 'azure', 'backend', 'bash', 'bitbucket', 
        'blockchain', 'books', 'bootstrap', 'bot', 'c', 'c#', 'c++', 'caching', 'career', 'chatbot', 
        'chrome', 'ci/cd', 'clojure', 'cloud', 'cmd', 'computerscience', 'container', 'crud', 
        'cryptocurrency', 'csharp', 'css', 'css3', 'csv', 'd3.js', 'dart', 'data structures', 
        'database', 'design', 'devops', 'disrtibuted', 'django', 'docker', 'dotnet', 'drupal', 
        'ecmascript', 'elasticsearch', 'electron', 'elixir', 'ember.js', 'erlang', 'es6', 'firebase', 
        'firefox', 'flask', 'flutter', 'framework', 'frontend', 'fullstack', 'gcc', 'git', 'github', 
        'gitlab', 'gnu', 'go', 'graphql', 'haskell', 'heroku', 'html', 'html5', 'http', 'https', 'interview', 
        'ionic', 'ios', 'iot', 'java', 'javascript', 'jenkins', 'jira', 'jquery', 'js', 'json', 'keras', 'kotlin', 
        'kubernetes', 'laravel', 'library', 'linux', 'machinelearning', 'matlab', 'maven', 'microservices', 
        'ml.net', 'mlpack', 'mongodb', 'monorepo', 'multithreading', 'mvc', 'mysql', 'netlify', 'networking', 
        'next.js', 'nlp', 'node', 'node.js', 'numpy', 'nuxt.js', 'oauth', 'objective-c', 'octave', 'oop', 'opencv', 
        'opengl', 'opensource', 'os', 'oss', 'pandas', 'perl', 'php', 'portfolio', 'postgressql', 'probability', 
        'productivity', 'programming', 'prototyping', 'pwa', 'pyspark', 'python', 'pytorch', 'qt', 'r', 'rails',
        'react', 'react-native', 'react.js', 'reactnative', 'redis', 'redux', 'regex', 'responsive', 'rest', 'ruby', 
        'rubyonrails', 'rust', 'safari', 'sass', 'scala', 'scss', 'security', 'selenium', 'seo', 'serverless', 
        'shell', 'software', 'solidity', 'spring', 'sql', 'statistics', 'stl', 'style', 'swift', 'swing', 
        'tensorflow', 'terminal', 'testing', 'travis', 'typescript', 'ubuntu', 'ui', 'unity', 'unix', 'unreal', 
        'ux', 'vb', 'vb.net', 'vim', 'visualbasic', 'vscode', 'vue', 'vue.js', 'web', 'web', 'webassembly', 
        'webdev', 'webview', 'windows', 'wordpress', 'xamarin', 'xml'
        ]


BASE_INDEED_URL = "https://www.monsterindia.com/srp/results?start={}&sort=2&limit=100&query=\"{}\"&jobFreshness=15&filter=true"

firebase = firebase.FirebaseApplication("https://monster-dd828.firebaseio.com/", None )

dataframe = pd.DataFrame(
        columns=["Search", "Title", "Location","Posted",  "Company", "Salary", "Description","Apply URL"])

start = [ "1", "100"]
for search in tags :
    for page in start:
        search_url_I = BASE_INDEED_URL.format(quote_plus(page), quote_plus(search))
        print(search_url_I)
        chrome_options = webdriver.ChromeOptions()
        chrome_options.binary_location = "/bin/chromium"
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--no-sandbox")
        driver = webdriver.Chrome("./chromedriver", options=chrome_options)
        driver.get(search_url_I)
        driver.implicitly_wait(5)


        all_jobs = driver.find_elements_by_class_name('job-apply-card')
        # print(all_jobs)
        for job in all_jobs:

            result_html = job.get_attribute('innerHTML')
            soup = BeautifulSoup(result_html, 'html.parser')
            # print(soup.prettify())

            post_url = soup.find("a")['href']
            l = list(post_url)
            p = l.index("/")  
            del(l[p])  
            p = l.index("/")  
            del(l[p])
            post_url = "".join(l)
            # print(post_url)

            try:
                post_posted = soup.find(class_="posted").text.replace("\n", "").strip()
            except:
                post_posted = 'None'

            try:
                title = soup.find("div", class_="job-tittle")
                post_title = title.h3.a.text
            except:
                post_title = 'None'  

            try:
                post_location = soup.small.text.replace("\n", "").strip()
            except:
                post_location = 'None'

            try:
                post_company = soup.find("a", "under-link").text.replace('\n', '')            
            except:
                post_company = 'Company Name Confidential'

            try:
                salary = soup.find("div", class_="package")
                post_salary = salary.span.small.text.replace("\n", "").strip()
            except:
                post_salary = 'Not Disclosed'

            try:
                post_job_desc = soup.find("p", class_="job-descrip").text.replace("\n", "").strip()
            except:
                post_salary = 'Not Disclosed'    
            data = {
                    'Search': search,
                    'Title': post_title, 
                    'Location': post_location,
                    "Posted": post_posted,  
                    "Company": post_company,
                    "Salary": post_salary, 
                    "Description": post_job_desc, 
                    "Apply URL": post_url
            }
            result = firebase.post('/monster-dd828/monster-dd828', data)
            dataframe = dataframe.append({'Search':search ,'Title': post_title, 'Location': post_location,"Posted": post_posted,  "Company": post_company,"Salary": post_salary, "Description": post_job_desc, "Apply URL": post_url},ignore_index=True)
            # print(dataframe)
                
        dataframe.to_csv("monster.csv",index=False)


PROCNAME = "chromedriver" # to clean up zombie ChromeDriver
for proc in psutil.process_iter():
# check whether the process name matches
    if proc.name() == PROCNAME:
        proc.kill()

# PROCNAME = "chromium" # to clean up zombie ChromeDriver
# for proc in psutil.process_iter():
# # check whether the process name matches
#     if proc.name() == PROCNAME:
#         proc.kill()
driver.close()
driver.quit()